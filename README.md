**Publication date:** 19.4.2016

**Submission deadline:** 

Genealogic web application
====================================
Whole documentation will be in english!
For inspiration https://familysearch.org/family-trees

## General information
// TODO

## Project structure
// TODO

## Authors
// TODO


======================================
## ZADÁNÍ
https://is.muni.cz/auth/el/1433/jaro2016/PB138/op/Hodnoceni_vyuky_a_organizace_projektu.html

### 2.4. Kontrolní body

Pro kontrolu průběhu řešení projektů jsou stanoveny tři kontrolní body:

* Zadání projektů – tento kontrolní bod ověřuje, že mají všichni studenti vybraný a založený projekt na GH, alespoň jednou aktivně použitou repozitory zdrojových kódů v GH (proveden aspoň jeden commit) a zveřejněnou první verzi wiki stránek projektu. Taktéž se ověřuje základní rozdělení rolí při řešení projektu, což musí být zřetelně rozepsáno na wiki a všichni řešitelé musejí být v projektu na GH vedeni jako vývojáři. Do projektu na GH je třeba jako "vývojáře" přidat i zadavatele projektu a přednášející (Tomáš Pitner, https://github.com/tpitner, https://gitlab.com/u/tpitner, Luděk Bártek, https://github.com/ludekbartek, Adam Rambousek https://github.com/rambousek).  Termín: 29. dubna 2016
* Zahájení řešení - tento kontrolní bod ověřuje, že tým už na projektu začal pracovat. Musí být hotový alespoň základní návrh aplikace a tento návrh musí být zdokumentován a zveřejněn na www stránkách projektu, pro něž lze využít integrovanou platformu na GH: https://pages.github.com/. Termín: 13. května 2016
* Finalizace vývoje - tento kontrolní bod ověřuje, že tým dokončil hlavní vývoj a že probíhá už pouze ladění, odstraňování chyb a implementace různých pomocných funkcí a vylepšení. Termín pro splnění těchto podmínek je 72 hodin před obhajobou projektu. Splnění se kontroluje při obhajobě.
* Dodržení kontrolních bodů může být ověřeno i zpětně pomocí systému Git, takže nezapomeňte příslušné materiály a zdrojové kódy pravidelně vkládat do repozitory a aktualizovat wiki/web projektu. Pro řízení projektu lze s výhodou využít systém pro sledování požadavků, chyb, oprav atd. (issue tracking) dostupný každému projektu na GC. Při vedení tímto způsobem se pak průběžný postup při obhajobě projektu dobře prokazuje.

### 2.5. Dokumentace

Každý projekt musí mít vhodně zpracovanou dokumentaci, volně dostupnou na wiki stránkách projektu. Každý řešitel navíc samostatně vypracuje závěrečnou zprávu o svém podílu na řešení projektu v rozsahu cca jedné stránky. Za dokumentaci se považují také dobře komentované zdrojové kódy. Pokud je výstupem projektu nějaké obecně použitelné API, musí být dokumentováno pomocí systému JavaDoc.

Závěrečné zprávy k projektu musí být primárně ve formátu DocBook, z něhož se potom generuje např. HTML nebo PDF, odkazovaný z webu projektu.

### 2.6. Obhajoby projektů a jejich hodnocení

Úspěšné vyřešení projektů budou studenti prokazovat jejich obhajobou. Obhajoby se budou konat ve zkouškového období a jejich datum i místo konání bude zveřejněno v informačním systému jako zkouškový termín.

Na obhajobu jednoho projektu je k dispozici 20 minut včetně dotazů i diskuse. Vlastní prezentace by neměla přesáhnout 15 minut, jinak bude přerušena a na obhajobu daného projektu bude nahlíženo jako na nedobře připravenou. Vedoucí týmu na začátku představí zadání projektu, řešitelský tým a stručně shrne dosažené výsledky. Poté vystoupí každý člen týmu s představením své části projektu, uvede použité technologie a nastíní architekturu své části systému. Na závěr proběhne diskuse. Součástí obhajoby by měla být elektronická prezentace výsledků (slidy, ukázky přístupné na síti, notebooku apod.).

Dejte si pozor, ať nezacházíte do zbytečných technických detailů. Zmínit se o konkrétních problémech, které bylo nutné vyřešit, je samozřejmě důležité, nicméně popisovat dlouhé popisovače aplikací ve formátu XML není dobrá strategie obhajoby.

Před obhajobou projektu musí být na www stránky projektu umístěny aktuální informace, finální verze programové části, dokumentace a zprávy k projektům. Na stránkách nesmí chybět ani všechny použité knihovny a jiné nástroje, které jsou potřebné pro překlad či používání nástroje. Samozřejmě není nutné přidávat instalační balíky s JDK nebo JRE.

======================================================
## Instruction for people in the project:

### How solution should work?
Mainly whole implementation should be in Java, HTML,CSS and Javascript
* We should consider using 
 * Sring JDBC framework for java
 * some CSS framework ?
 * Grunt, Node.js for javascript
    
Also should we read data from XML or from the database? 

### TODOS
* ALL - figure out login and security, CREATE GITLAB ACCOUNT PLEASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* Martin - sort out doumentation and style displaying members (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* Daniel + Nicol - create basic backend for our application (CRUD, servlets, listeners in web app,...)
* Hung - create design and front end of the application